package com.efaceless.web.web;

import com.efaceless.web.entity.Emotion;
import com.efaceless.web.service.EmotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
public class EmotionController {
    private EmotionService emotionService;

    @Autowired
    public void setEmotionService(EmotionService emotionService) {
        this.emotionService = emotionService;
    }

    @GetMapping("/api/emotion")
    public ResponseEntity<List<Emotion>> getAllEmotions() {
        List<Emotion> list = emotionService.retrieveEmotions();
        return new ResponseEntity<List<Emotion>>(list, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/emotion", method = RequestMethod.POST)
    public ResponseEntity<Object> createEmotion(@RequestBody Emotion emotion) {
        //Set Date Create.
        emotion.setCreationDateTime(new Date());
        emotionService.save(emotion);
        return new ResponseEntity<>("{\"message\":\"Emotion is created successfully\"}", HttpStatus.CREATED);
    }

    @GetMapping("/api/emotion/desc")
    public ResponseEntity<List<String>> getEmotionDescs(){
        List<String> list = emotionService.retrieveEmotionDescs();
        return new ResponseEntity<List<String>>(list,HttpStatus.OK);
    }

    @GetMapping("/api/emotion/type")
    public ResponseEntity<List<String>> getEmotionType() {
        List<String> list = emotionService.retrieveEmotionTypes();
        return new ResponseEntity<List<String>>(list, HttpStatus.OK);
    }
}
