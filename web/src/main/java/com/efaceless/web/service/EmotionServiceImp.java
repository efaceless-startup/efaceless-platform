package com.efaceless.web.service;

import com.efaceless.web.entity.Emotion;
import com.efaceless.web.repository.EmotionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmotionServiceImp implements EmotionService{
    @Autowired
    EmotionRepository emotionRepository;

    public List<Emotion> retrieveEmotions(){
        return (List<Emotion>)emotionRepository.findAll();
    }

    @Override
    public List<String> retrieveEmotionDescs(){
        return (List<String>) emotionRepository.findAllDescs();
    }

    @Override
    public void save(Emotion emotion) {
        emotionRepository.save(emotion);
    }

    @Override
    public List<String> retrieveEmotionTypes() {
        return (List<String>) emotionRepository.findAllTypes();
    }
}
